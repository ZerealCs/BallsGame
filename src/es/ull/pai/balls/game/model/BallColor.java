/**
 * BallsGame - BallColor.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.model;

/**
 * Colors applied to balls, no one other is should use
 *
 */
public enum BallColor {
    Red,
    Blue,
    Yellow,
    Violet,
    Cyan,
    Green
}
