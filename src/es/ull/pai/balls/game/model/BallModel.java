/**
 * BallsGame - BallModel.java 8/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.model;

import es.ull.pai.balls.game.utils.Tuple;

/**
 * TODO: Commenta algo
 *
 */
public class BallModel {
    private Tuple<Double> pos;
    private double radius;
    private BallColor color;

    /**
     *
     */
    public Tuple<Double> getPos() {
        return pos;
    }

    public BallModel setPos(Tuple<Double> pos) {
        this.pos = pos;
        return this;
    }

    /**
     *
     */
    public double getRadius() {
        return radius;
    }

    public BallModel setRadius(double radius) {
        this.radius = radius;
        return this;
    }

    /**
     *
     */
    public BallColor getColor() {
        return color;
    }

    public BallModel setColor(BallColor color) {
        this.color = color;
        return this;
    }
}
