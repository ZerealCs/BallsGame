/**
 * BallsGame - Collision.java 8/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.model;

import es.ull.pai.balls.game.utils.Tuple;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Implements a detection of collision between walls and other balls
 *
 */
public class Collision {
    /**
     * Collision with a wall?
     * TODO: Implement point of collision with wall
     * @param ball ball to check
     * @param pos pos of wall
     * @param size size of wall
     * @return collision success
     */
    public static Optional<WallCollision> collisionWalls(BallModel ball, Tuple<Double> pos, Tuple<Double> size) {

        if (ball.getPos().getY() - ball.getRadius() / 2.0 < pos.getY()
                || ball.getPos().getY() + ball.getRadius() / 2.0 > size.getY()
                || ball.getPos().getX() - ball.getRadius() / 2.0 < pos.getX()
                || ball.getPos().getX() + ball.getRadius() / 2.0 > size.getX()) {
            return Optional.of(new WallCollision(ball, new Tuple<>(0.0, 0.0)));
        }
        else {
            return Optional.empty();
        }
    }

    /**
     * Collision with other balls?
     * @param ball ball to check
     * @param ballModels other balls
     * @return collision success
     */
    public static Optional<BallCollision> collisionBalls(BallModel ball, ArrayList<BallModel> ballModels) {
        ArrayList<BallModel> balls = new ArrayList<>();
        for (BallModel ball2 : ballModels) {
            if (Math.sqrt(Math.pow(ball2.getPos().getX() - ball.getPos().getX(), 2)
                    + Math.pow(ball2.getPos().getY() - ball.getPos().getY(), 2)) < ball.getRadius()) {
                balls.add(ball2);
            }
        }
        if (balls.size() >= 1) {
            return Optional.of(new BallCollision(ball, balls));
        }
        else {
            return Optional.empty();
        }
    }
}
