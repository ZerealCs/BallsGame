/**
 * BallsGame - Launch.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.model;


import es.ull.pai.balls.game.utils.Tuple;

/**
 * TODO: Commenta algo
 *
 */
public class Launch {
    private Tuple<Double> initialPos;
    private Tuple<Double> speed;

    public Launch(double x, double y, double speed, double angle) {
        initialPos = new Tuple<>(x, y);
        this.speed = new Tuple<Double>(Math.cos(angle) * speed, Math.sin(angle) * speed);
    }

    public Tuple<Double> currentPos(double time) {
        return new Tuple<Double>(getInitialPos().getX() + getSpeed().getX() * time
                                , getInitialPos().getY() + getSpeed().getY() * time);
    }

    /**
     *
     */
    public Tuple<Double> getInitialPos() {
        return initialPos;
    }

    public Launch setInitialPos(Tuple<Double> initialPos) {
        this.initialPos = initialPos;
        return this;
    }

    /**
     *
     */
    public Tuple<Double> getSpeed() {
        return speed;
    }

    public Launch setSpeed(Tuple<Double> speed) {
        this.speed = speed;
        return this;
    }
}
