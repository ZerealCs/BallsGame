/**
 * BallsGame - WallCollision.java 10/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.model;

import es.ull.pai.balls.game.utils.Tuple;

/**
 * TODO: Commenta algo
 *
 */
public class WallCollision extends Collision {
    private BallModel ball;
    private Tuple<Double> pointCollision;

    public WallCollision(BallModel ball, Tuple<Double> pointCollision) {
        this.ball = ball;
        this.pointCollision = pointCollision;
    }

    /**
     * Get ball that collisioned with wall
     */
    public BallModel getBall() {
        return ball;
    }

    /**
     * Point exact of collision
     */
    public Tuple<Double> getPointCollision() {
        return pointCollision;
    }
}
