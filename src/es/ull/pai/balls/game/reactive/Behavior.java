/**
 * BallsGame - Behavior.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.reactive;

/**
 * Represents a behavior, a value that varies on continuous time.
 */
public class Behavior<V> {
    private V value;
    private Event<V> internalEvent = new Event<>();

    /**
     * Creates a a behavior with a initial value
     * @param initialValue
     */
    public Behavior(V initialValue) {
        value = initialValue;
        internalEvent.send(initialValue);
        internalEvent.onDispatch(this::setValue); // update value in function of event
    }

    public Behavior(V initialValue, Event<V> updater) {
        value = initialValue;
        internalEvent = updater;
        internalEvent.send(initialValue);
        internalEvent.onDispatch(this::setValue); // update value in function of event
    }

    /**
     * Get a value in any moment
     * @return value
     */
    public V getSample() {
        return value;
    }

    /**
     * Get update event of behavior
     * @return
     */
    public Event<V> updates() {
        return internalEvent;
    }

    private Behavior setValue(V value) {
        this.value = value;
        return this;
    }
}
