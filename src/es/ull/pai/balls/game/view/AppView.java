/**
 * BallsGame - AppView.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.view;

import es.ull.pai.balls.game.Resources;
import es.ull.pai.balls.game.model.BallCollision;
import es.ull.pai.balls.game.model.Collision;
import es.ull.pai.balls.game.model.Launch;
import es.ull.pai.balls.game.model.WallCollision;
import es.ull.pai.balls.game.reactive.Behavior;
import es.ull.pai.balls.game.reactive.Event;
import es.ull.pai.balls.game.utils.Tuple;

import javax.swing.*;


import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Optional;

/**
 * Join all views into this class.
 */
public class AppView extends JPanel {
    // Constants
    private final Color BACKGROUND_COLOR = Color.decode("#b6fbff");
    private final int FPS = 1000 / 60;

    // Attributes / variables
    private Optional<Timer> timer = Optional.empty();
    private Optional<Ball> optBall = Optional.empty();
    private Launch launch;
    private Canvas canvas = new Canvas();
    private Balls balls = new Balls(0);
    private Cannon cannon = new Cannon(0.0);

    // Events/Behaviors
    private Behavior<Double> time = new Behavior<>(0.0);
    private Event<Collision> collision = new Event<>();

    /**
     * Make a view
     */
    public AppView() {
        // Setting variables
        setLayout(new BorderLayout());
        add(getCanvas(), BorderLayout.CENTER);

        setUpInfo();
        setUpCanvas();
        setUpCannon();
        setUpTimeEvent();
        setUpCollisions();
    }

    /**
     * Action on collision
     */
    private void setUpCollisions() {
        // Wall Collisions
        getCollision()
                .filter(collision1 -> collision1 instanceof WallCollision)
                .onDispatch(collision1 -> resetBall());

        // Balls collisions
        getCollision()
                .filter(collision1 -> collision1 instanceof BallCollision)
                .map(collision1 -> (BallCollision) collision1)
                .filter(ballCollision -> !ballCollision.isDoubleImpact())
                .onDispatch(ballCollision -> {
                    resetBall();            // Reset It if not the ball continue to end way
                    Resources.getInstance().getBlink().play();
                    getBalls().removeBall(ballCollision.getBallImpact());
        });
    }

    /**
     * Set up time event when launch a ball
     */
    private void setUpTimeEvent() {
        // Update elements each time
        getTime().updates().onDispatch(aDouble -> {
            getOptBall().ifPresent(ball -> {
                ball.setPos(getLaunch().currentPos(aDouble));

                Collision.collisionWalls(ball.getModel()
                                        , new Tuple<>(0.0, -ball.getDiameter())
                                        , new Tuple<>((double)getWidth(), (double)getHeight()))
                        .ifPresent(getCollision()::send);

                Collision.collisionBalls(ball.getModel(), getBalls().getModels()).ifPresent(getCollision()::send);
            });
            getCanvas().repaint();
        });
    }

    /**
     * Set up cannon to target to mouse pos
     */
    private void setUpCannon() {
        // Set cannon to mouse direction
        getCannon().setAngle(getCanvas().getMouse()
                .map(mouseEvent -> getAngle(mouseEvent.getPoint().getX(), mouseEvent.getPoint().getY()))
                .hold(0.0)
        );
    }

    /**
     * Reset ball
     */
    public void resetBall() {
        timer.ifPresent(Timer::stop);
        timer = Optional.empty();
        optBall = Optional.of(new Ball().setPos(new Tuple<>(getCanvas().getWidth() / 2.0, 0.0)));
    }

    /**
     * Determine the angle with a point. The origin its set "width / 2"
     * @param x
     * @param y
     * @return
     */
    public double getAngle(double x, double y) {
        double y2 = getCanvas().getHeight() - y;
        double x2 = getCanvas().getWidth() / 2.0 - x;

        return Math.atan2(y2, -x2);
    }

    /**
     * Make a info button.
     */
    public void setUpInfo() {
        final String INFO_TITLE = "Information";
        final String INFO = "Práctica 1100\n"
                + "Descripción: Lanzas bolas, impactan y destruyen\n"
                + "Universidad de La Laguna para la asignatura \"PAI\"\n\n"
                + "Made with hate by Eleazar Díaz Delgado\n"
                + "Contact: https://github.com/EleDiaz";
        final int SIZE_ICON = 24;
        JButton button = new JButton(Resources.getInstance().getInfoIcon());
        button.setPreferredSize(new Dimension(SIZE_ICON, SIZE_ICON));
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                JOptionPane.showMessageDialog(null, INFO, INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
            }
        });
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel, BorderLayout.SOUTH);
    }

    /**
     * Set differents shapes to draw over canvas
     */
    public void setUpCanvas() {
        // Draw oven canvas
        getCanvas().getPaint()
                .onDispatch(g -> {
                    g.setColor(BACKGROUND_COLOR);
                    g.fillRect(0, 0, getCanvas().getWidth(), getCanvas().getHeight());

                    // Draw cannon
                    getCannon().setPos(new Tuple<>(getCanvas().getWidth() / 2.0, 0.0))
                            .draw(g);
                    // Draw balls
                    getBalls().setPos(new Tuple<>(getCanvas().getWidth() % getBalls().getDiameter() / 2
                                                 , getCanvas().getHeight() - getBalls().getDiameter()))
                            .draw(g);

                    // Draw launch ball if it exist
                    getOptBall().ifPresent(ball -> ball.draw(g));
                });

        // If screen dimension is change, add new balls to fixed to screen
        getCanvas().getDimension().updates().onDispatch(size -> {
            getBalls().setNumBalls((int) (size.getX() / getBalls().getDiameter()));
        });

        // Put ball on middle
        getCanvas().getDimension().updates().onDispatch(size ->
                setOptBall(Optional.of(new Ball().setPos(new Tuple<>(size.getX() / 2.0, 0.0))))
        );

        // On mouse click launch a ball
        getCanvas().getClick()
                // Delete click events when is launch a ball or not it is the key specific of mouse
                .filter(mouseEvent -> !getTimer().isPresent() && mouseEvent.getButton() == MouseEvent.BUTTON1)
                .onDispatch(mouseEvent -> {
                    setLaunch(new Launch(getCanvas().getWidth() / 2.0, 0, 1, getAngle(mouseEvent.getX(), mouseEvent.getY())));
                    //setOptBall(Optional.of(new Ball().setPos(getLaunch().currentPos(0))));

                    double initialTime = System.currentTimeMillis();
                    setTimer(Optional.of(new Timer(FPS, actionEvent ->
                        getTime().updates().send(System.currentTimeMillis() - initialTime))));
                    getTimer().get().start();
                });
    }

    /**
     *
     */
    public Canvas getCanvas() {
        return canvas;
    }

    public AppView setCanvas(Canvas canvas) {
        this.canvas = canvas;
        return this;
    }

    /**
     *
     */
    public Balls getBalls() {
        return balls;
    }

    public AppView setBalls(Balls balls) {
        this.balls = balls;
        return this;
    }

    /**
     *
     */
    public Cannon getCannon() {
        return cannon;
    }

    public AppView setCannon(Cannon cannon) {
        this.cannon = cannon;
        return this;
    }

    /**
     *
     */
    public Launch getLaunch() {
        return launch;
    }

    public AppView setLaunch(Launch launch) {
        this.launch = launch;
        return this;
    }

    /**
     *
     */
    public Optional<Ball> getOptBall() {
        return optBall;
    }

    public AppView setOptBall(Optional<Ball> optBall) {
        this.optBall = optBall;
        return this;
    }

    /**
     *
     */
    public Event<Collision> getCollision() {
        return collision;
    }

    public AppView setCollision(Event<Collision> collision) {
        this.collision = collision;
        return this;
    }

    /**
     *
     */
    public Behavior<Double> getTime() {
        return time;
    }

    public AppView setTime(Behavior<Double> time) {
        this.time = time;
        return this;
    }

    /**
     *
     */
    public Optional<Timer> getTimer() {
        return timer;
    }

    public AppView setTimer(Optional<Timer> timer) {
        this.timer = timer;
        return this;
    }
}
