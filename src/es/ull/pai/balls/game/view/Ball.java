/**
 * BallsGame - BallModel.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.view;

import es.ull.pai.balls.game.model.BallColor;
import es.ull.pai.balls.game.model.BallModel;
import es.ull.pai.balls.game.reactive.Behavior;
import es.ull.pai.balls.game.utils.Tuple;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * TODO: Commenta algo
 *
 */
public class Ball extends DrawerComponent {
    // Constants
    private static final List<BallColor> VALUES =
            Collections.unmodifiableList(Arrays.asList(BallColor.values()));
    private static final int SIZE_COLOR = VALUES.size();
    private static final Random RANDOM = new Random();

    // Variables
    private double diameter = 50;
    private Tuple<Double> pos = new Tuple<>(0.0, 0.0);
    private Behavior<BallColor> color;

    public Ball() {
        color = new Behavior<BallColor>(VALUES.get(RANDOM.nextInt(SIZE_COLOR)));
    }

    @Override
    public void draw(Graphics2D graphics2D) {
        double DEGREE_CIRCLE = 360.0;
        AffineTransform mat = graphics2D.getTransform();
        Arc2D circle = new Arc2D.Double(0.0, 0.0, SIZE.getX(), SIZE.getY(), 0.0, DEGREE_CIRCLE, Arc2D.OPEN);
        graphics2D.translate(getPos().getX() - getDiameter() / 2, getPos().getY() - getDiameter() / 2);
        graphics2D.scale(getDiameter(), getDiameter());
        graphics2D.setColor(transform(color.getSample()));
        graphics2D.fill(circle);
        graphics2D.setTransform(mat);
    }

    private Color transform(BallColor color) {
        final Color VIOLET = Color.decode("#7F00FF");
        switch (color) {
            case Blue:
                return Color.BLUE;
            case Red:
                return Color.RED;
            case Yellow:
                return Color.YELLOW;
            case Green:
                return Color.GREEN;
            case Cyan:
                return Color.CYAN;
            case Violet:
                return VIOLET;
        }
        return Color.BLACK;
    }

    public BallModel getModel() {
        return new BallModel().setPos(pos).setRadius(diameter);
    }

    /**
     *
     */
    public Behavior<BallColor> getColor() {
        return color;
    }

    public Ball setColor(Behavior<BallColor> color) {
        this.color = color;
        return this;
    }

    /**
     *
     */
    public double getDiameter() {
        return diameter;
    }

    public Ball setDiameter(double diameter) {
        this.diameter = diameter;
        return this;
    }

    /**
     *
     */
    public Tuple<Double> getPos() {
        return pos;
    }

    public Ball setPos(Tuple<Double> pos) {
        this.pos = pos;
        return this;
    }
}
